package com.antoinecampbell.lambda.graal.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.antoinecampbell.lambda.graal.item.Item;
import com.antoinecampbell.lambda.graal.item.ItemDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

public class GraalApiHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private ObjectMapper objectMapper;
    private ItemDao itemDao;

    public GraalApiHandler() {
        objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        itemDao = new ItemDao();
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        try {
            System.out.println(String.format("Input %s", objectMapper.writeValueAsString(request)));
            List<Item> items = itemDao.getItems();
            response.setBody(objectMapper.writeValueAsString(items));
            response.setStatusCode(200);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                response.setBody(objectMapper.writeValueAsString(Collections.singletonMap("error", e.getMessage())));
            } catch (JsonProcessingException ex) {
                ex.printStackTrace();
            }
            response.setStatusCode(500);
        }

        return response;
    }

    /**
     * Used for local testing of the handler
     */
    public static void main(String[] args) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            // Get lambda request event
            String runtimeApiEndpoint = System.getenv("AWS_LAMBDA_RUNTIME_API");
            String requestUrl = String.format("http://%s/2018-06-01/runtime/invocation/next", runtimeApiEndpoint);
            requestUrl = "http://www.google.com";

            URL url = new URL(requestUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.getInputStream();
            String requestId = httpURLConnection.getHeaderField("lambda-runtime-aws-request-id");

            APIGatewayProxyResponseEvent response = new GraalApiHandler().handleRequest(null, null);
            String responseString = objectMapper.writeValueAsString(response);

            requestUrl = String.format("http://%s/2018-06-01/runtime/invocation/%s/response",
                    runtimeApiEndpoint, requestId);
            url = new URL(requestUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            try(OutputStream os = httpURLConnection.getOutputStream()) {
                byte[] input = responseString.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }
            try(InputStream stream = httpURLConnection.getInputStream()) {
            }

//            httpRequest = HttpRequest.newBuilder()
//                    .method("POST", HttpRequest.BodyPublishers.ofString(responseString))
//                    .uri(URI.create(requestUrl))
//                    .build();
//            httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
//            lambdaRequest = new Request.Builder()
//                    .post(RequestBody.create( MediaType.parse("application/json"), responseString))
//                    .url(requestUrl)
//                    .build();
//            client.newCall(lambdaRequest).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
