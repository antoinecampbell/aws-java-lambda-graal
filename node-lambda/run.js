const {handler} = require('./index');

// Used for local testing of the handler
handler({}, {}).then(response => {
    console.log('Output', response);
    console.log('Body', JSON.parse(response.body));
}).catch(error => console.log(error));