const {Client} = require('pg');
const SSM = require('aws-sdk/clients/ssm');

const databaseHost = process.env['DATASOURCE_HOST'] || 'localhost';
const databasePort = process.env['DATASOURCE_PORT'] || 5432;
const databaseName = process.env['DATASOURCE_DATABASE'] || 'root';
const functionName = process.env['AWS_LAMBDA_FUNCTION_NAME'] || 'node-lambda';
const ssmClient = new SSM({apiVersion: '2014-11-06'});
let username, password;

exports.handler = async function (event, context) {
    console.log('Input', event);
    let response;
    try {
        if (!username || !password) {
            // Fetch credentials when not already set
            const credentials = await getCredentials();
            username = credentials.username;
            password = credentials.password;
        }
        const client = new Client({
            connectionString: `postgresql://${username}:${password}@${databaseHost}:${databasePort}/${databaseName}?ApplicationName=${functionName}`
        });
        await client.connect();
        let results = {};
        await client.query('SELECT * FROM item')
            .then(result => {
                results = result.rows.map(row => ({
                    id: row['item_id'],
                    name: row['name'],
                    createBy: row['created_by'],
                    lastModifiedBy: row['last_modified_by'],
                    createdDate: row['created_date'],
                    lastModifiedDate: row['last_modified_date'],
                }))
            })
            .catch(error => {
                console.log(error);
                results = {
                    error
                }
            });
        await client.end();

        response = {
            statusCode: 200,
            body: JSON.stringify(results)
        }
    } catch (e) {
        console.error('Error', e);
        response = {
            statusCode: 500,
            body: JSON.stringify({
                error: e.message
            })
        }
    }

    return response;
};

async function getCredentials() {
    const usernameParam = process.env['DATASOURCE_USERNAME'] || 'root';
    const passwordParam = process.env['DATASOURCE_PASSWORD'] || 'password';
    if (!isLocalhost()) {
        // Get database credentials from SSM when not running locally
        const ssmResponse = await ssmClient.getParameters({
            Names: [usernameParam, passwordParam],
            WithDecryption: true
        }).promise();
        const username = ssmResponse.Parameters.find(param => param.Name === usernameParam).Value;
        const password = ssmResponse.Parameters.find(param => param.Name === passwordParam).Value;
        return {
            username,
            password
        }
    }

    return {
        username: usernameParam,
        password: passwordParam
    }
}

function isLocalhost() {
    return databaseHost.indexOf('localhost') !== -1;
}