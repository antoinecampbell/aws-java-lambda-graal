locals {
  java_lambda_zip_location   = "../java-lambda/build/distributions/java-lambda.zip"
  node_lambda_zip_location   = "../node-lambda/build/distributions/node-lambda.zip"
  python_lambda_zip_location = "../python-lambda/build/distributions/python-lambda.zip"

  database_name            = "root"
  get_items_function_name  = "ac-java11-graal-test-driver"
  get_items2_function_name = "ac-java11-graal-test-connection-pool"
  get_items3_function_name = "ac-node12-test-node-driver"
  get_items4_function_name = "ac-python-test-driver"

  database_properites = {
    DATASOURCE_HOST     = aws_db_instance.db.address
    DATASOURCE_PORT     = aws_db_instance.db.port
    DATASOURCE_DATABASE = local.database_name
    DATASOURCE_USERNAME = aws_ssm_parameter.db_username.name
    DATASOURCE_PASSWORD = aws_ssm_parameter.db_password.name
  }
}

resource "aws_lambda_function" "get_items" {
  function_name = local.get_items_function_name
  role          = aws_iam_role.lambda_exec.arn
  runtime       = "java11"

  handler          = "com.antoinecampbell.lambda.graal.handler.ApiHandler::handleRequest"
  filename         = local.java_lambda_zip_location
  source_code_hash = filebase64sha256(local.java_lambda_zip_location)

  timeout     = 15
  memory_size = 512

  reserved_concurrent_executions = 30

  vpc_config {
    security_group_ids = [data.aws_security_group.default.id]
    subnet_ids         = data.aws_subnet_ids.default.ids
  }

  environment {
    variables = merge(local.database_properites, {
      USE_CONNECTION_POOL = "false"
    })
  }
}

resource "aws_cloudwatch_log_group" "get_item" {
  name = "/aws/lambda/${local.get_items_function_name}"

  retention_in_days = 14
}

resource "aws_lambda_function" "get_items2" {
  function_name = local.get_items2_function_name
  role          = aws_iam_role.lambda_exec.arn
  runtime       = "java11"

  handler          = "com.antoinecampbell.lambda.graal.handler.ApiHandler::handleRequest"
  filename         = local.java_lambda_zip_location
  source_code_hash = filebase64sha256(local.java_lambda_zip_location)

  timeout     = 15
  memory_size = 512

  reserved_concurrent_executions = 30

  vpc_config {
    security_group_ids = [data.aws_security_group.default.id]
    subnet_ids         = data.aws_subnet_ids.default.ids
  }

  environment {
    variables = merge(local.database_properites, {
      USE_CONNECTION_POOL = "true",
    })
  }
}

resource "aws_cloudwatch_log_group" "get_item2" {
  name = "/aws/lambda/${local.get_items2_function_name}"

  retention_in_days = 14
}

resource "aws_lambda_function" "get_items3" {
  function_name = local.get_items3_function_name
  role          = aws_iam_role.lambda_exec.arn
  runtime       = "nodejs12.x"

  handler          = "index.handler"
  filename         = local.node_lambda_zip_location
  source_code_hash = filebase64sha256(local.node_lambda_zip_location)

  timeout     = 15
  memory_size = 128

  reserved_concurrent_executions = 30

  vpc_config {
    security_group_ids = [data.aws_security_group.default.id]
    subnet_ids         = data.aws_subnet_ids.default.ids
  }

  environment {
    variables = local.database_properites
  }
}

resource "aws_cloudwatch_log_group" "get_item3" {
  name = "/aws/lambda/${local.get_items3_function_name}"

  retention_in_days = 14
}

resource "aws_lambda_function" "get_items4" {
  function_name = local.get_items4_function_name
  role          = aws_iam_role.lambda_exec.arn
  runtime       = "python3.6"

  handler          = "index.handler"
  filename         = local.python_lambda_zip_location
  source_code_hash = filebase64sha256(local.python_lambda_zip_location)

  timeout     = 15
  memory_size = 128

  reserved_concurrent_executions = 30

  vpc_config {
    security_group_ids = [data.aws_security_group.default.id]
    subnet_ids         = data.aws_subnet_ids.default.ids
  }

  environment {
    variables = local.database_properites
  }
}

resource "aws_cloudwatch_log_group" "get_item4" {
  name = "/aws/lambda/${local.get_items4_function_name}"

  retention_in_days = 14
}