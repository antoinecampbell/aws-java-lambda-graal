variable "region" {}
variable "ip_address" {}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_security_group" "default" {
  name = "default"
}

resource "aws_db_instance" "db" {
  identifier          = "ac-test-db"
  allocated_storage   = 20
  storage_type        = "gp2"
  engine              = "postgres"
  engine_version      = "10.6"
  instance_class      = "db.t2.micro"
  name                = "root"
  username            = "root"
  password            = "password"
  publicly_accessible = true
  skip_final_snapshot = true
}

resource "aws_ssm_parameter" "db_username" {
  name  = "/ac/db/username"
  value = aws_db_instance.db.username
  type  = "SecureString"
}

resource "aws_ssm_parameter" "db_password" {
  name  = "/ac/db/password"
  value = aws_db_instance.db.password
  type  = "SecureString"
}

resource "aws_vpc_endpoint" "ssm" {
  service_name        = "com.amazonaws.${var.region}.ssm"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true

  vpc_id             = data.aws_vpc.default.id
  security_group_ids = [data.aws_security_group.default.id]
  subnet_ids         = data.aws_subnet_ids.default.ids
}