output "api_url" {
  value = aws_api_gateway_deployment.stage.invoke_url
}

output "daatabase_endpoint" {
  value = aws_db_instance.db.endpoint
}