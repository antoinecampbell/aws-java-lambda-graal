package com.antoinecampbell.lambda.graal.item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Builder
public class Item {

    private Long id;
    private String name;
    private String createdBy;
    private String lastModifiedBy;
    private ZonedDateTime createdDate;
    private ZonedDateTime lastModifiedDate;
}
