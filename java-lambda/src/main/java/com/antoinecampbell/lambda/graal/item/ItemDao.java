package com.antoinecampbell.lambda.graal.item;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ItemDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemDao.class);

    @Setter
    private DataSource dataSource;
    @Setter
    private SsmClient ssmClient;

    public ItemDao() {
    }

    public List<Item> getItems() throws Exception {
        List<Item> items = new ArrayList<>();

        try (Connection connection = getConnection();
             ResultSet resultSet = connection.prepareCall("SELECT * FROM item").executeQuery()) {
            while (resultSet.next()) {
                Item item = Item.builder()
                        .id(resultSet.getLong("item_id"))
                        .name(resultSet.getString("name"))
                        .createdBy(resultSet.getString("created_by"))
                        .lastModifiedBy(resultSet.getString("last_modified_by"))
                        .createdDate(ZonedDateTime.ofInstant(
                                resultSet.getTimestamp("created_date")
                                        .toLocalDateTime().toInstant(ZoneOffset.UTC), ZoneId.of("UTC")))
                        .lastModifiedDate(ZonedDateTime.ofInstant(
                                resultSet.getTimestamp("last_modified_date")
                                        .toLocalDateTime().toInstant(ZoneOffset.UTC), ZoneId.of("UTC")))
                        .build();
                items.add(item);
            }
        }

        return items;
    }

    private void initWithConnectionPool() throws Exception {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        Credentials credentials = getCredentials();
        hikariConfig.setUsername(credentials.getUsername());
        hikariConfig.setPassword(credentials.getPassword());
        hikariConfig.setJdbcUrl(getJdbcUrl());
        hikariConfig.setMaximumPoolSize(Integer.parseInt(
                Optional.ofNullable(System.getenv("MAX_POOL_SIZE")).orElse("1")));
        hikariConfig.setMinimumIdle(Integer.parseInt(
                Optional.ofNullable(System.getenv("MIN_IDLE")).orElse("0")));
        hikariConfig.setIdleTimeout(Long.parseLong(
                Optional.ofNullable(System.getenv("IDLE_TIMEOUT")).orElse("10000")));
        dataSource = new HikariDataSource(hikariConfig);
    }

    private void initWithDriver() throws Exception {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setURL(getJdbcUrl());
        Credentials credentials = getCredentials();
        dataSource.setUser(credentials.getUsername());
        dataSource.setPassword(credentials.getPassword());

        this.dataSource = dataSource;
    }

    private DataSource getDataSource() throws Exception {
        if (dataSource == null) {
            boolean useConnectionPool = Boolean.parseBoolean(
                    Optional.ofNullable(System.getenv("USE_CONNECTION_POOL"))
                            .orElse("false"));
            if (useConnectionPool) {
                initWithConnectionPool();
            } else {
                initWithDriver();
            }
        }
        return dataSource;
    }

    private Credentials getCredentials() throws Exception {
        if (isLocalhost()) {
            String username = Optional.ofNullable(System.getenv("DATASOURCE_USERNAME"))
                    .orElse("root");
            String password = Optional.ofNullable(System.getenv("DATASOURCE_PASSWORD"))
                    .orElse("password");
            return new Credentials(username, password);
        } else {
            LOGGER.debug("Start: fetching SSM params");
            String usernameParam = Objects.requireNonNull(
                    System.getenv("DATASOURCE_USERNAME"), "Username param is missing");
            String passwordParam = Objects.requireNonNull(
                    System.getenv("DATASOURCE_PASSWORD"), "Password param is missing");
            GetParametersRequest getParametersRequest = GetParametersRequest.builder()
                    .names(usernameParam, passwordParam)
                    .withDecryption(true)
                    .build();

            GetParametersResponse getParametersResponse = getSsmClient().getParameters(getParametersRequest);
            String username = getParametersResponse.parameters().stream()
                    .filter(parameter -> usernameParam.equals(parameter.name()))
                    .findFirst()
                    .orElseThrow(() -> new Exception("Username param is missing from response"))
                    .value();
            String password = getParametersResponse.parameters().stream()
                    .filter(parameter -> passwordParam.equals(parameter.name()))
                    .findFirst()
                    .orElseThrow(() -> new Exception("Password param is missing from response"))
                    .value();
            LOGGER.debug("End: fetching SSM params");

            return new Credentials(username, password);
        }
    }

    private Connection getConnection() throws Exception {
        return getDataSource().getConnection();
    }

    private SsmClient getSsmClient() {
        if (ssmClient == null) {
            ssmClient = SsmClient.create();
        }
        return ssmClient;
    }

    private boolean isLocalhost() {
        return Optional.ofNullable(System.getenv("DATASOURCE_HOST"))
                .orElse("localhost")
                .contains("localhost");
    }

    private String getJdbcUrl() {
        String host = Optional.ofNullable(System.getenv("DATASOURCE_HOST"))
                .orElse("localhost");
        String port = Optional.ofNullable(System.getenv("DATASOURCE_PORT"))
                .orElse("5432");
        String databaseName = Optional.ofNullable(System.getenv("DATASOURCE_DATABASE"))
                .orElse("root");
        String functionName = Optional.ofNullable(System.getenv("AWS_LAMBDA_FUNCTION_NAME"))
                .orElse("java-lambda");
        return String.format("jdbc:postgresql://%s:%s/%s?ApplicationName=%s",
                host, port, databaseName, functionName);
    }

    @Getter
    @AllArgsConstructor
    private static class Credentials {
        private final String username;
        private final String password;
    }
}
