package com.antoinecampbell.lambda.graal.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.antoinecampbell.lambda.graal.item.Item;
import com.antoinecampbell.lambda.graal.item.ItemDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class ApiHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiHandler.class);

    private ObjectMapper objectMapper;
    private ItemDao itemDao;

    public ApiHandler() {
        objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        itemDao = new ItemDao();
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        try {
            LOGGER.debug("Input {}", objectMapper.writeValueAsString(request));
            List<Item> items = itemDao.getItems();
            response.setBody(objectMapper.writeValueAsString(items));
            response.setStatusCode(200);
        } catch (Exception e) {
            LOGGER.error("Error", e);
            try {
                response.setBody(objectMapper.writeValueAsString(Collections.singletonMap("error", e.getMessage())));
            } catch (JsonProcessingException ex) {
                LOGGER.error("Error", ex);
            }
            response.setStatusCode(500);
        }

        return response;
    }

    /**
     * Used for local testing of the handler
     */
    public static void main(String[] args) {
        APIGatewayProxyResponseEvent response = new ApiHandler().handleRequest(null, null);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            LOGGER.debug(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(response));
            LOGGER.debug(objectMapper.readTree(response.getBody()).toPrettyString());
        } catch (JsonProcessingException e) {
            LOGGER.error("Error", e);
        }
    }
}
