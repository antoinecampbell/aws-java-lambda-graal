# aws-java-lambda-graal
Lambda project to test out GraalVM with a Java AWS Lambda.

## Test Results
### Cold Lambdas
![](reports/cold-lambda-10-threads-100-requests/Screen%20Shot%202019-11-30%20at%206.38.38%20PM.png)

### Warm Lambdas
![](reports/warm-lambdas-10-threads-100-requests/Screen%20Shot%202019-11-30%20at%206.38.27%20PM.png)

## Build Packages
```bash
./gradlew build
```

## Terraform Deploy
```bash
export AWS_PROFILE=profile-name
export AWS_DEFAULT_REGION=us-east-1
terraform apply -var-file test.tfvars -var ip_address=my-ip-address
```

## Run JMeter Test Plan
```bash
jmeter -n -t test-plan.jmx \
    -e -o build/jmeter-report \
    -l build/test.jtl \
    -Jserver.host=api-gateway-domain
```

## Build Native Image in Docker

```bash
docker run --rm -it  -v `pwd`:`pwd` \
    -w `pwd` \
    oracle/graalvm-ce /bin/bash

gu install native-image
yum install java-11-openjdk-devel -y
native-image --enable-url-protocols=http \
    -H:+ReportUnsupportedElementsAtRuntime \
    -H:+ReportExceptionStackTraces \
    -H:+TraceClassInitialization \
    -H:ReflectionConfigurationFiles=reflect.json \
    --allow-incomplete-classpath \
    --no-fallback \
    --no-server \
    -jar build/libs/java-lambda-graal.jar
```

## Build Native Image Locally

```bash
~/Desktop/graalvm-ce-java11-19.3.0/Contents/Home/bin/native-image --enable-url-protocols=http \
    -H:+ReportUnsupportedElementsAtRuntime \
    -H:+ReportExceptionStackTraces \
    -H:+TraceClassInitialization \
    -H:ReflectionConfigurationFiles=reflect.json \
    -H:Name=java-graal-image \
    --initialize-at-build-time=org.slf4j.helpers.NOPLoggerFactory \
    --initialize-at-build-time=org.slf4j.LoggerFactory \
    --initialize-at-build-time=org.slf4j.helpers.SubstituteLoggerFactory \
    --initialize-at-build-time=org.slf4j.helpers.Util \
    --initialize-at-run-time=io.netty.util.internal.logging.Log4JLogger \
    --allow-incomplete-classpath \
    --no-fallback \
    --no-server \
    -jar build/libs/java-lambda-graal.jar
```

The python lambda must be built inside a linux container due to the postgres binary being os specific
```bash
docker run --rm -it -v `pwd`:`pwd` -w `pwd` lambci/lambda:build-python3.6 /bin/bash
```