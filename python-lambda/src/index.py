import json
import os

import boto3
from sqlalchemy import create_engine, Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

database_host = os.getenv("DATASOURCE_HOST", "localhost")
database_port = os.getenv("DATASOURCE_PORT", "5432")
database_name = os.getenv("DATASOURCE_DATABASE", "root")
username = os.getenv("DATASOURCE_USERNAME", "root")
password = os.getenv("DATASOURCE_PASSWORD", "password")
function_name = os.getenv("AWS_LAMBDA_FUNCTION_NAME", "python-lambda")
if "localhost" not in database_host:
    ssm_client = boto3.client("ssm")
    ssm_response = ssm_client.get_parameters(
        Names=[username, password],
        WithDecryption=True
    )
    for param in ssm_response['Parameters']:
        if param['Name'] == username:
            username = param['Value']
        if param['Name'] == password:
            password = param['Value']

url = f"postgresql+psycopg2://{username}:{password}@{database_host}:{database_port}/{database_name}" \
      f"?application_name={function_name}"
engine = create_engine(url, poolclass=NullPool)
Session = sessionmaker(bind=engine)

Base = declarative_base()


def handler(event, context):
    print(f"Input: {event}")

    s = Session()
    items = [item.serialize() for item in s.query(Item).all()]
    s.close()

    return {
        "statusCode": 200,
        "body": json.dumps(items)
    }


class Item(Base):
    __tablename__ = 'item'

    item_id = Column(Integer, primary_key=True)
    name = Column(String(50))
    created_by = Column(String(50))
    last_modified_by = Column(String(50))
    created_date = Column(DateTime)
    last_modified_date = Column(DateTime)

    def serialize(self):
        return {
            'id': self.item_id,
            'name': self.name,
            'createdBy': self.created_by,
            'createdDate': self.created_date.isoformat(),
            'lastModifiedBy': self.last_modified_by,
            'lastModifiedDate': self.last_modified_date.isoformat()
        }
