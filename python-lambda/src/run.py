import json

from src.index import handler

# Used for local testing of the handler
if __name__ == '__main__':
    response = handler({}, {})
    print(response)
    print(json.dumps(json.loads(response['body']), indent=2))
