from setuptools import setup

setup(
    name="python-lambda",
    version="0.1",

    install_requires=[
        "psycopg2-binary",
        "sqlalchemy",
        "boto3==1.9.221"
    ],

    tests_require=[
        "pytest"
    ]
)
