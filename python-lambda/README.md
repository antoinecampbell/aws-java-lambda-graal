# python-lambda

## Local Setup
Create virtual environment, from python-lambda directory
```bash
python3 -m venv .venv
```

Activate virtual environment, from python-lambda directory
```bash
source .venv/bin/activate
```

Install dependencies, from python-lambda directory
```bash
python setup.py install
```

## Setup to build for AWS
Inside of a linux docker container, from python-lambda directory
```bash
python3 -m venv .linuxvenv
```

Activate virtual environment, from python-lambda directory
```bash
source .linuxvenv/bin/activate
```

Install dependencies, from python-lambda directory
```bash
python setup.py install
```

Build lambda zip, from root directory
```bash
./gradlew python-lambda buildZip
```